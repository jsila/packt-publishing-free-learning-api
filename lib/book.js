const fs = require('fs')

let filepath

module.exports.setFilepath = f => {
  filepath = f
}

module.exports.get = () => {
  let b = require(filepath)
  if (b.time) {
    b.time = new Date(b.time)
  }
  return b
}

module.exports.set = title => {
  let b = { title, time: new Date()}
  fs.writeFile(filepath, JSON.stringify(b))
  return b
}
