const request = require('request')
const cheerio = require('cheerio')

module.exports.get = cb => {
  request('https://www.packtpub.com/packt/offers/free-learning', (error, resp, body) => {
    if (error || resp.statusCode !== 200) {
      cb('Problem retriving URL content', undefined)
      return
    }
    let $ = cheerio.load(body)
    let title = $('.dotd-title h2').text().trim()
    cb(undefined, title)
  })
}

module.exports.check = (date1, date2) => {
  let newBookDate = new Date()
  newBookDate.setHours(2)
  newBookDate.setMinutes(0)
  newBookDate.setSeconds(0)

  return date1 < newBookDate && newBookDate < date2
}
