const express = require('express')
const basicAuth = require('express-basic-auth')
const helmet = require('helmet')
const {resolve} = require('path')

const book = require('./lib/book')
const scrape = require('./lib/scrape')

book.setFilepath(resolve('book.json'))

let data = book.get()

let app = express()

app.use(require('express-toobusy')())

app.use(helmet())

app.use(basicAuth({
    users: {'jernej': 'jernejjezakon'}
}))

app.get('/', (req, res) => {
  if (data.time && !scrape.check(data.time, new Date())) {
    return res.json({ title: data.title })
  }
  scrape.get((error, title) => {
    if (error) {
      return res.json({ error })
    }
    data = book.set(title)
    res.json({ title })
  })
})

const PORT = 3000
const ADDRESS = '0.0.0.0'

let server = app.listen(PORT, ADDRESS, () => {
  console.log('Serving at http://%s:%d', server.address().address, server.address().port)
})
